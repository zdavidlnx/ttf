import numpy as np
from matplotlib import pyplot as plt
from scipy.io.wavfile import write
from scipy.fft import fft, fftfreq

SAMPLE_RATE = 44100  # Hertz
DURATION = 5  # Seconds


def generate_sine_wave(freq, sample_rate, duration):
    xs = np.linspace(0, duration, sample_rate * duration, endpoint=False)
    frequencies = xs * freq
    # 2pi because np.sin takes radians
    ys = np.sin((2 * np.pi) * frequencies)
    return xs, ys


def save_wav_file(filename, sample_rate, normalized_tone_to_save):
    # Remember SAMPLE_RATE = 44100 Hz is our playback rate, normalmente
    # filename .wav
    write(filename, sample_rate, normalized_tone_to_save)


if __name__ == "__main__":      
    # Generate a 2 hertz sine wave that lasts for 5 seconds
    x, y = generate_sine_wave(2, SAMPLE_RATE, DURATION)
    plt.plot(x, y)
    plt.show()

    # Se generan 2 ondas uno de 400Hz y un ruido de 4000Hz
    _, nice_tone = generate_sine_wave(400, SAMPLE_RATE, DURATION)
    _, noise_tone = generate_sine_wave(4000, SAMPLE_RATE, DURATION)
    noise_tone = noise_tone * 0.3

    mixed_tone = nice_tone + noise_tone
    normalized_tone = np.int16((mixed_tone / mixed_tone.max()) * 32767)

    plt.plot(normalized_tone[:1000])
    plt.show()

    # guardamos como .wav
    save_wav_file("sonido.wav", SAMPLE_RATE, normalized_tone)
